#!/bin/sh
# reset jetbrains ide evals

OS_NAME=$(uname -s)
JB_PRODUCTS="IntelliJIdea CLion PhpStorm GoLand PyCharm WebStorm Rider DataGrip RubyMine AppCode"

if [ $OS_NAME == "Darwin" ]; then
	echo 'macOS:'

	for PRD in $JB_PRODUCTS; do
    	rm -rf ~/Library/Preferences/${PRD}*/eval
    	rm -rf ~/Library/Application\ Support/JetBrains/${PRD}*/eval
    	rm -rf ~/Library/Application\ Support/JetBrains/${PRD}*/options/other.xml
        
        #My part - based on https://dstarod.github.io/idea-trial/
        cd ~/Library/Preferences/
        rm -rf com.apple.java.util.prefs.plist
        rm -rf .intelli*
        rm -rf idea*
        rm -rf jetbrains.*
        rm -rf CLion*
        rm -rf IdeaIC*

        cd ~/Library/Application\ Support/JetBrains/
        rm -rf IntelliJIdea*-backup
        cd IntelliJIdea*
        rm -rf eval
        cd options
        rm -rf other.xml

        # Otional -  I not used it, but is recommended to delete too
        # Based on https://dstarod.github.io/remove-idea-completely/
        ls -d -1 /Applications/* ~/Library/Preferences/* \
        ~/Library/Caches/* ~/Library/Application\ Support/* \
        ~/Library/Logs/* | grep -i "jetbrains\|idea" | xargs rm -rf

	done
elif [ $OS_NAME == "Linux" ]; then
	echo 'Linux:'

	for PRD in $JB_PRODUCTS; do
    	rm -rf ~/.${PRD}*/config/eval
	done
else
	echo 'unsupport'
	exit
fi

echo 'done.'
