# Scripts

It's my scripts for automatization of routine actions

[![](https://habrastorage.org/files/f70/4ea/a92/f704eaa92cb649ba8ab929c1d17b9a9f.jpg)]



### Categories:
1) DevOps
2) Reset trials and remove application data

### Running
My favorite os for servers is CentOS (RedHat). And all scripts will be writted considering what will be running in this OS.
For debian based systems you must will replace 'yum' on 'apt-get'. 
For MacOS on 'brew'.
For Windows on 'chocolate' or 'winget' (I not tested and not used for my targets windows-based systems)


### Important
Don't forget disable "selinux" and "firewalld" or docker not will work
