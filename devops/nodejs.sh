#!/bin/sh

# Install nodeJS 15.10
sudo su
yum install -y git
curl https://raw.githubusercontent.com/creationix/nvm/v0.35.3/install.sh | bash
source ~/.bashrc
nvm install v15.10

# Actual for some oses
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm
