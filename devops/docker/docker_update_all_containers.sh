/*
Based on https://dev.to/goffity/update-all-docker-images-already-pulled-o3l
*/
docker images | grep -v REPOSITORY | awk '{print $1}' | xargs -L1 docker pull
