#!/bin/sh

sudo su
yum install -y docker
systemctl start docker
systemctl enable docker
groupadd docker
usermod -aG docker $USER
docker run hello-world

docker run -d -p 9001:9001 --name portainer_agent --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/docker/volumes:/var/lib/docker/volumes portainer/agent
