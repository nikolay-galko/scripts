# Jenkins initial install
#!/bin/sh

echo 'Installation of Jenkins'

sudo su

# Latest versions of jenkins is required Java 11
yum install -y java-11-openjdk-devel git wget
wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo
rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
yum install -y jenkins

systemctl enable jenkins