# Jenkins: script for checking is directory has changed from last build
# Original: https://gist.github.com/fesor/9465dd9bb02741579fa2
# FYI:
# GIT_PREVIOUS_COMMIT=$(git rev-parse --short "HEAD^")
# GIT_COMMIT=$(git rev-parse --short HEAD)
# So you don't have to rely on Jenkins

#!/bin/bash

DIR_PATH=$1
if [ ! -d "$DIR_PATH" ]; then
    echo "Directory '$DIR_PATH' not exists"
    exit 1
fi

if [ -z "$GIT_COMMIT" ]; then
    echo "No current commit... fail"
    exit 1
fi

if [ -z "$GIT_PREVIOUS_COMMIT" ]; then
    echo "No previous commit, files are changed!"
    exit 0
fi

# Check is files in given directory changed between commits
# NOTE: $GIT_PREVIOUS_COMMIT and $GIT_COMMIT provided by Jenkins GIT Plugin
CHANGED=`git diff --name-only $GIT_PREVIOUS_COMMIT $GIT_COMMIT $DIR_PATH`

if [ -z "$CHANGED" ]; then
    echo "No changes dettected..."
else
    echo "Directory changed"
fi
