# Script for install Android SDK to CentOS
# based on: https://gist.github.com/jpswade/33841e261b28073d9e7551922acea1f2
# and : https://github.com/mahiso/AndroidSDKCentOS7


#!/bin/sh
yum install java -y
yum install android-tools -y

cd /opt

mkdir sdks
cd /sdks

mkdir -p android-sdk-linux
cd android-sdk-linux

# @see https://developer.android.com/studio/index.html
wget --output-document=android-sdk.zip --quiet https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip
unzip android-sdk.zip
rm -f android-sdk.zip
yes | tools/bin/sdkmanager --licenses
tools/bin/sdkmanager "tools" "platform-tools" "extras;google;m2repository"

echo 'export ANDROID_HOME=/opt/sdks/android-sdk-linux' >> ~/.bashrc
echo 'export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools' >> ~/.bashrc

#let's check
cat ~/.bashrc
