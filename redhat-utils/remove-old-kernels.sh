# Run it as root

echo 'Script for removing older kernels'
sudo su

# Fedora and modern distrs way
dnf remove $(dnf repoquery --installonly --latest-limit 1 -q) 

# Old systems
#yum install yum-utils -y
#package-cleanup --oldkernels --count=2