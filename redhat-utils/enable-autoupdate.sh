# Run it as root

# Modern RedHat systems
# read https://haydenjames.io/enable-automatic-updates-fedora-red-hat-centos-bonus-tip/
# https://www.cyberciti.biz/faq/install-enable-automatic-updates-rhel-centos-8/

echo 'Configuration Autoupdates for RHEL'

sudo su

dnf install dnf-automatic nano -y
# change apply_updates = no to apply_updates = yes
nano /etc/dnf/automatic.conf

systemctl enable --now dnf-automatic.timer

# Old RedHat systems
yum install -y yum-cron nano
nano /etc/yum/yum-cron.conf
# change apply_updates = no to apply_updates = yes