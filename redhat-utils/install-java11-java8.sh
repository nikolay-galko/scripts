# Run it as root

echo 'Installation of Java 11 or Java 8'
sudo su

# Manual: https://wiki.merionet.ru/servernye-resheniya/43/ustanovka-oracle-java-11-i-openjdk-11-v-centos-8-i-rhel-8/
yum install java-11-openjdk-devel
java -version


# Optionally 1

# If needed
yum install java-1.8.0-openjdk-devel

# Set default version of java, for multiply versions
alternatives --config java

# Optionally 2
echo 'export PATH=$PATH:/usr/lib/jvm/java-11-openjdk-11.0.2.7-2.el8.x86_64/bin/
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.2.7-2.el8.x86_64/
export J2SDKDIR=/usr/lib/jvm/java-11-openjdk-11.0.2.7-2.el8.x86_64/' >> /etc/profile.d/java.sh
source /etc/profile.d/java.sh